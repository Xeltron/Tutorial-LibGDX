# Estructura Modelo-Vista-Controlador

Este tipo de diseño facilita la ordenación del código de manera que sea fácil de 
programar, de mantener y de leer. Además permite intercambiar fácilmente sus 
compenentes por otros por lo que esto lo hace extrapolable.

Para más información sobre el concepto visita el siguiente enlace: [link] (https://es.wikipedia.org/wiki/Modelo%E2%80%93vista%E2%80%93controlador)

## Controlador

En este caso, sería la clase encargada de gestionar el ciclo de vida del juego.

Esta clase deberá implentar la interfaz "ApplicationListener" o la clase "Game". 
La única diferencia entre estas dos maneras distintas de crear nuestro controlador 
es que la clase "Game" ya implementa los métodos de dicha interfaz.

Hechemos un vistazo a los métodos de nuestro controlador:


- public void create ();

Es el encargado de inicializar el modelo y la vista (en este orden). Además también 
podemos inicializar algunas variables auxiliares.

- public void resize (int width, int height);

Cada vez que el usuario cambie el tamaño de la pantalla llamará a este método y 
nos servirá para actualizar dichos tamaños pasandoselos a la vista.


- public void render ();

Este método es uno de los más importantes debido a que se llamará constantemente 
para actualizar el modelo y la vista. 

Para que sea más fácil de imaginar se trataría de un bucle el cuál siempre se 
ejecutará mientras la aplicación permanezca viva.

- public void pause ();

Método exclusivo de versiones móviles. Se llamará cuando el usuario reciba una 
llamada entrante por ejemplo.

Deberemos tener especial cuidado cuando utilizemos recursos visuales ya que estos
deberán volver a ser inicializados.

- public void resume ();

Método exclusivo de versiones móviles. Se llamará cuando el usuario vuelva a la 
aplicación.

En este método es donde volveremos a inicializar los recursos visuales para 
tenerlos cargados y seguir utilizandolos.

- public void dispose ();

Encargado de liberar toda la memoría del dispositivo cerrando cada uno de los 
recursos utilizados.

<br/>
## Vista

Se encargará de dibujar todos los elementos del juego. Como parte de este tipo 
de diseño, contendrá en su interior una referencia hacia el modelo. El objetivo 
de esto es simple, para dibujar cada elemento en la zona y manera indicadas 
deberá recoger estos datos en dicha clase.


Deberá implementar la interfaz "Disposable". Esto obligará a que contenga el 
método "dispose" el cuál tendrá el mismo objetivo que el que hemos explicado 
antes de la clase controlador. El objetivo de esto es generar una cadena de 
llamadas para asegurarnos que todos los recursos son liberados.

Además, es recomendable añadir en esta clase dos métodos principales más. Uno de 
ellos deberá inicializar los recursos que necesitará más adelante nuestra clase 
Vista (método que llamaríamos desde el método "create" de la clase controlador").
El otro se trata de un método que se encargara de pintar la pantalla cada vez que 
sea llamado (y este otro en el "render").


<br/>
## Modelo

Nuestro modelo de negocio en este caso se tratará de la lógica del videojuego.
Todos los cálculos del juego deberán ejecutarse desde esta clase. En otras 
palabras, aquí es donde se define el comportamiento del juego y se establecen 
las "leyes" de este. Algunos ejemplos sería calcular la velocidad o las físicas 
de un elemento en movimiento, la reacción de un enemigo al activar un evento o 
controlar el tiempo y la puntuación de una partida.

Además, esta clase podemos hace que descienda de "InputAdapter" para recoger de
manera directa cada acción que haga el usuario. Apretar una tecla, soltarla, 
mover el ratón, clickar o tocar la pantalla, pulsar el boton X de un mando... 
Todas estas acciones serían claros ejemplos de eventos.
