# Estructura básica de un proyecto con LibGDX

## Proyecto inicial

A la hora de ejecutar el .jar de libGDX de instalación nos habrá generado la siguiente estrucutra:


![alt text](res/archivosBasicosLibGDX.png)

<br/>

Ahora vamos a entrar en detalle sobre algunos de los archivos y carpetas más importantes.


##### Carpetas

**android/**:   Proyecto Android

**core/**:  Proyecto central *Nota: Aquí es donde escribiremos la mayor parte del código*

**desktop/**:   Proyecto Escritorio

**html/**:  Proyecto HTML5

**ios/**:   Proyecto iOs



##### Archivos

**build.gradle**:   Aquí encontraremos las dependencias globales y de cada proyecto.

**gradelw**:    Permite compilar el proyecto desde terminal con *./gradelw [nombreModulo]:run*

**local.properties**:   Guarda la ruta del SDK y el NDK de android


<br/>

##### Rutas útiles

**android/assets/**:    En esta carpeta se guardarán todos los recursos media que utilizará el 
juego independientemente de la plataforma en la que se este ejecutando.


