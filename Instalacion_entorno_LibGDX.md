Descargar jar libgdx
====================
Primero de todo tenemos que descargar el .jar de libgdx que nos permitirá poder crear el proyecto, una vez lo ejecutemos.

[Jar Libgdx](https://libgdx.badlogicgames.com/download.html "Jar Libgdx")

Definir propietario workspace
=============================
Importante si vamos a trabajar con Android Studio a la hora de crear un directorio de trabajo, asignar nuestro usuario como propietario, para ello utilizaremos

chow -R iamXXXXXXXX nombreCarpeta

Ejecutar jar libgdx
===================
Para poder ejecutar nuestros jar que hemos descargado, accedemos desde la terminal al directorio donde lo tengamos y ejecutamos 

java -jar nombreJar

Una vez lo ejecutado veremos los siguiente:

![alt text](res/JarLibgdx.png)


Ahora pasaremos a definir los parametros necesarios para crear el proyecto.
- 
Name:Nombre del proyecto

Package:Package del proyecto

Game class:Nombre de la clase principal del proyecto

Destination:Indicamos el workspace de trabajo que anteriormente hemos configurado

Android SDK:Indicamos donde se encuentra el directorio de nuestra SDK

Sub Projects:Seleccionamos las plataformas que queramos urilizar para nuestro proyecto

Extensions:Por ahora quitamos Box2d

Y pulsamos Generate


Abrir proyecto libgdx con Android Studio
========================================
Para poder abrir el proyecto que hemos generado con el jar de libgdx, vamos a Android Studio y seleccionamos open project e indicamos el directorio donde se encuentra la aplicación libgdx creada por el jar.



Configurar launcher desktop
===========================
Para lanzar nuestra aplicación en escritorio y no como aplicación Android hacemos lo siguiente:

Accedemos a Run > Edit Configurations... en el menu

Añadimos una nueva configuración y seleccionamos la opción de 'Application' y editamos los campos siguiente

Name: Nombre de la nueva configuración

Main class: com.xxx.desktop.DesktopLauncher

Working directory: /opt/Project_Libgdx_Prueba/android/assets   <----- Definimos donde se encuentran los assets

Use classpath of mod: seleccionamos el modulo desktop