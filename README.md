# Tutorial sobre LibGDX en Android Studio

## Indice

1. [Instalación entorno LibGDX](https://gitlab.com/Xeltron/Tutorial-LibGDX/blob/master/Instalacion_entorno_LibGDX.md)
2. [Estructura básica](https://gitlab.com/Xeltron/Tutorial-LibGDX/blob/master/Estructura_basica.md)
3. [Modelo-Vista-Controlador](https://gitlab.com/Xeltron/Tutorial-LibGDX/blob/master/Modelo_Vista_Controlador.md)
